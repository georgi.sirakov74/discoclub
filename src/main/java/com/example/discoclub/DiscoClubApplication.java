package com.example.discoclub;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DiscoClubApplication {

    public static void main(String[] args) {
        SpringApplication.run(DiscoClubApplication.class, args);
    }

}
